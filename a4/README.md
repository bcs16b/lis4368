> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development

## Bernard Schramm

### Assignment 4 Requirements:

*Three Parts:*

1. Modify Customer.java
2. Modify customerform.jsp
3. Modify CustomerServlet.java

#### README.md file should include the following items:

* Screenshot of failed validation;
* Screenshot of passed validation;

#### Assignment Screenshots:

| *Screenshot of Failed Validation: | *Screenshot of Passed Validation: |
| ------------------------------------------- | ------------------------------------------- |
| ![Failed validation](img/failed.png) | ![Passed validation](img/passed.png) |
| ------------------------------------------- | ------------------------------------------- |