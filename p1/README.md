> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development

## Bernard Schramm

### Project 1 Requirements:

*Three Parts:*

1. Modify index.jsp
2. Add form controls
3. Add jQuery validation 

#### README.md file should include the following items:

* Screenshot of carousel;
* Screenshot of invalid form submission;
* Screenshot of valid form submission;

#### Assignment Screenshots:

![Screenshot of carousel](img/home.png)

![Screenshot of invalid form submission](img/invalid.png)

![Screenshot of valid form submission](img/valid.png)

local lis4368 web app: http://localhost:9999/lis4368/