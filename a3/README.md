
# LIS4381 - Advanced Web App Development

## Bernard Schramm

### Assignment 3 Requirements:

1. Entity Relationship Diagram (ERD)
2. Include data (At least 10 records in each table)

#### README.md file should include the following items:

* Screenshot of A3 ERD
* Link to a3.sql file
* Link to a3.mwb file

#### Assignment Screenshots:

|*Screenshot of ERD*: |
|--------------------------|
|![Screenshot of ERD](img/a3.png)|

[A3 SQL File](docs/a3.sql "A3 SQL Script")
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
