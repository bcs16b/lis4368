> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 - Advanced Web Application Development

## Bernard Schramm

### LIS 4368 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create and populate a database in mySQL
    - Java Servlet Development
    - Provide screenshots of localhost

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD)
    - Include data (At least 10 records in each table)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Modify Customer.java
    - Modify customerform.jsp
    - Modify CustomerServlet.java

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Modify Customer.java
    - Modify customerform.jsp
    - Modify CustomerServlet.java

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Modify index.jsp
    - Add form controls
    - Add jQuery validation 

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify Customer.java
    - Modify customerform.jsp
    - Modify CustomerServlet.java