> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development

## Bernard Schramm

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running Java Hello (#1 above);
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/short descriptions;
* Bitbucket repo links 

	a) this assignment and 

	b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates an empty git repository or reinitialize an existing one
2. git status - displays paths that have differences between the index file and>
3. git add - add file contents to the index using the current content on the wo>
4. git commit - records changes to the repository
5. git push - updates remote refs along with associated objects using local refs
6. git pull - incorporates changes from a remote repository into the current br>
7. git clone - clones a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*

![Tomcat Installation Screenshot](img/tomcat.png)

local lis4368 web app: http://localhost:9999/lis4368/

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bcs16b/bitbucketstationlocations/ "Bitbucket Station Locations")

