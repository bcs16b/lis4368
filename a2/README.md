> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development

## Bernard Schramm

### Assignment 2 Requirements:

*Three Parts:*

1. Create and populate a database in mySQL
2. Java Servlet Development
3. Chapter Questions 

#### README.md file should include the following items:

* Screenshot of running localhost:9999/hello (directory listing);
* Screenshot of running localhost:9999/hello (index.html);
* Screenshot of running localhost:9999/hello/sayhello;
* Screenshot of running localhost:9999/hello/querybook.html;
* Screenshot of query results.

#### Assignment Screenshots:

http://localhost:9999/hello

**Note:** directory below will be listed \*only\* if there is no **index.html** file, otherwise, see page below.

![localhost/hello Directory Listing Screenshot](img/directory_listing.png)

http://localhost:9999/hello/index.html

**Note:** Using **index.html** will correctly hide directory files!

![localhost/hello index Screenshot](img/index.png)

http://localhost:9999/hello/sayhello

![localhost/hello/sayhello Screenshot](img/using_servlets.png)

http://localhost:9999/hello/querybook.html

![localhost/hello/querybook Screenshot](img/database_connectivity1.png)

**query results**

![localhost/hello/querybook Results Screenshot](img/database_connectivity2.png)

local lis4368 web app: http://localhost:9999/lis4368/
